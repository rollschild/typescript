var u = undefined;
var n = null;
// null and undefined are subtypes of all other types
// NOT assignable to types other than void and their own types
// ...when --strictNullChecks
// let str: string = undefined; // will ERROR if the flag is set
var anotherStr = undefined;
