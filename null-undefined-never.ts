let u: undefined = undefined;
let n: null = null;

// null and undefined are subtypes of all other types
// NOT assignable to types other than void and their own types
// ...when --strictNullChecks
// let str: string = undefined; // will ERROR if the flag is set
let anotherStr: string | undefined = undefined; // good

// Function returning *never* must have unreachable end point
function error(message: string): never {
  throw new Error(message);
}
