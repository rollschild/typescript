function f(_a) {
    var a = _a.a, b = _a.b;
    console.log(a, b);
}
f({ a: 'hello', b: 2037 });
// Specifying defaults is tricky:
function foo(_a) {
    var _b = _a === void 0 ? { a: '' } : _a, a = _b.a, _c = _b.b, b = _c === void 0 ? 0 : _c;
    console.log(a, b);
}
foo({ a: 'wonderful' });
foo();
// foo({}); // will ERROR
