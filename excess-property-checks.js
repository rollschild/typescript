// ...and so long as they are not color or width,
// ...their types do not matter
function createSquare(config) {
    return { color: config.color, area: config.width * config.width };
}
var mySquare = createSquare({ colour: 'purple', width: 12 });
console.log(mySquare);
var squareOptions = { colour: 'green', width: 11 };
var newSquare = createSquare(squareOptions);
console.log(newSquare);
