interface ArrayOfString {
  [index: number]: string;
}
// when it's indexed with a number, it returns a string

let oneArray: ArrayOfString = ['Jovi', 'Eva', 'Wendy'];

let oneString = oneArray[0];
let someString = oneArray[1];
let anotherString = oneArray[2];
console.log(oneString, someString, anotherString);
