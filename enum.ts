enum Car {
  Audi = 16,
  Mazda = 2037,
  Toyota,
  BMW,
}
let car: Car = Car.Mazda;
console.log(car);
let anotherCarName: string = Car[2038]; // search

// ERROR:
// type *string* NOT assignable to type *Car*
// let anotherCar: Car = Car[2038];
// console.log(anotherCar);
console.log(anotherCarName);
