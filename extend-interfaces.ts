interface Shape {
  color: string;
}

interface PenStroke {
  penWidth: number;
}

interface Square extends Shape, PenStroke {
  sideLength: number;
}

let square = <Square>{};
square.sideLength = 12;
square.color = 'purple';
square.penWidth = 0.5;

console.log(square);
