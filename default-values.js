function keepWholeObject(wholeObj) {
    var a = wholeObj.a, _a = wholeObj.b, b = _a === void 0 ? 2037 : _a;
    console.log(a);
    // *b* is optional
    // question mark *?* indicates this argument is optional
}
keepWholeObject({ a: 'hello', b: 16 });
