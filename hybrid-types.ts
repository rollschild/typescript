interface Counter {
  (start: number): string;
  interval: number;
  reset(): void;
}

function getCounter(): Counter {
  let counter = <Counter>function(start: number) {
    let res = 'start at: ' + start;
    return res;
  };
  counter.interval = 7;
  counter.reset = function() {};
  return counter;
}

let cntr = getCounter();
cntr(2037);
cntr.reset();
cntr.interval = 5.0;
console.log('counter: ', cntr(2037));
