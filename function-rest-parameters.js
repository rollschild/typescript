function buildName(lastName) {
    var otherNames = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        otherNames[_i - 1] = arguments[_i];
    }
    return otherNames.join(' ') + ' ' + lastName;
}
console.log(buildName('Shi', 'Guangchu', 'Jovi'));
var nameBuilder = buildName;
console.log(nameBuilder('Wu', 'Junyao', 'Miko'));
