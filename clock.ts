interface ClockConstructor {
  new (hour: number, minute: number): ClockInterface;
}

interface ClockInterface {
  tick(): void;
}

function creatClock(
  ctor: ClockConstructor,
  hour: number,
  minute: number,
): ClockInterface {
  return new ctor(hour, minute);
}

class DigitalClock implements ClockInterface {
  constructor(h: number, m: number) {}

  tick() {
    console.log('dududu!');
  }
}

class AnalogClock implements ClockInterface {
  constructor(h: number, m: number) {}

  tick() {
    console.log('tick tock tick tock...');
  }
}

let digital = creatClock(DigitalClock, 20, 37);
let analog = creatClock(AnalogClock, 08, 59);
digital.tick();
analog.tick();
