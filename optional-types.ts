enum InputError {
  NoInput,
  Invalid,
}

enum DayOfWeek {
  Sunday,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
}

class Either<TLeft, TRight> {
  private readonly value: TLeft | TRight;
  private readonly right: boolean;

  constructor(value: TLeft | TRight, right: boolean) {
    this.value = value;
    this.right = right;
  }

  isRight(): boolean {
    return this.right;
  }

  isLeft(): boolean {
    return !this.right;
  }

  getRight(): TRight {
    if (!this.isRight()) throw new Error();

    return <TRight>this.value;
  }

  getLeft(): TLeft {
    if (!this.isLeft()) throw new Error();

    return <TLeft>this.value;
  }

  static makeRight<TLeft, TRight>(value: TRight): Either<TLeft, TRight> {
    return new Either<TLeft, TRight>(value, true);
  }

  static makeLeft<TLeft, TRight>(value: TLeft): Either<TLeft, TRight> {
    return new Either<TLeft, TRight>(value, false);
  }
}

type Result = Either<InputError, DayOfWeek>;
