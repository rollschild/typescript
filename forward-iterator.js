class LinkedListNode {
    constructor(value) {
        this.value = value;
    }
}
class LinkedListIterator {
    constructor(node) {
        this.node = node;
    }
    get() {
        if (this.node == undefined)
            throw Error();
        return this.node.value;
    }
    increment() {
        if (this.node == undefined)
            throw Error();
        this.node = this.node.next;
    }
    set(value) {
        if (this.node == undefined)
            throw Error();
        this.node.value = value;
    }
    clone() {
        return new LinkedListIterator(this.node);
    }
    equals(other) {
        return this.node == other.node;
    }
}
function find(begin, end, pred) {
    while (!begin.equals(end)) {
        if (pred(begin.get())) {
            return begin;
        }
        begin.increment();
    }
    return end;
}
