type IteratorResult<T> = {
  done: boolean;
  value: T;
};

interface Iterator<T> {
  next(): IteratorResult<T>;
}

class BinaryTreeNode<T> {
  value: T;
  left: BinaryTreeNode<T> | undefined;
  right: BinaryTreeNode<T> | undefined;

  constructor(value: T) {
    this.value = value;
  }
}

class BinaryTreeNodeIterator<T> implements Iterator<T> {
  private values: T[];
  private root: BinaryTreeNode<T>;

  constructor(root: BinaryTreeNode<T>) {
    this.values = [];
    this.root = root;

    this.inOrder(root);
  }

  next(): IteratorResult<T> {
    const result: T | undefined = this.values.shift();

    if (!result) {
      return {
        done: true,
        value: result,
      };
    }

    return {
      done: false,
      value: result,
    };
  }

  private inOrder(node: BinaryTreeNode<T>): void {
    if (node.left != undefined) {
      this.inOrder(node.left);
    }

    this.values.push(node.value);

    if (node.right != undefined) {
      this.inOrder(node.right);
    }
  }
}

function* inOrderIterator<T>(root: BinaryTreeNode<T>): IterableIterator<T> {
  if (root.left) {
    for (const value of inOrderIterator(root.left)) {
      yield value;
    }
  }

  yield root.value;

  if (root.right) {
    for (const value of inOrderIterator(root.right)) {
      yield value;
    }
  }
}
