var Student = /** @class */ (function () {
    // *public* is a shorthand to automatically create
    // ...properties with that name
    function Student(firstName, middleInitial, lastName) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.fullName = firstName + ' ' + middleInitial + ' ' + lastName;
    }
    return Student;
}());
function greeter(person) {
    return 'Hello, ' + person.firstName + ' ' + person.lastName;
}
var user = new Student('Guangchu', 'Jovi', 'Shi');
document.body.innerHTML = greeter(user);
