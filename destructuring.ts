type C = {a: string; b?: number};
function f({a, b}: C): void {
  console.log(a, b);
}
f({a: 'hello', b: 2037});

// Specifying defaults is tricky:
function foo({a, b = 0} = {a: ''}): void {
  console.log(a, b);
}
foo({a: 'wonderful'});
foo();
// foo({}); // will ERROR
