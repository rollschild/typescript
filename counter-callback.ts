type AsyncFunction = () => void;

let queue: AsyncFunction[] = [];

function countDown(counterId: string, from: number, callback: () => void): void {
  console.log(`${counterId}: ${from}`);

  if (from > 0) {
    queue.push(() => countDown(counterId, from - 1, callback));
  } else {
    queue.push(callback);
  }
};

queue.push(() => countDown("counterTwo", 6, () => console.log("Done")));

while (queue.length > 0) {
  let func: AsyncFunction = <AsyncFunction>queue.shift();
  func();
}
