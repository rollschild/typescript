class Person {
  protected name: string;
  constructor(name: string) {
    this.name = name;
  }
}

class Employee extends Person {
  private department: string;

  constructor(name: string, department: string) {
    super(name);
    this.department = department;
  }

  // members declared *protected* can be accessed within deriving classes
  public selfIntro() {
    return `Hi, my name is ${this.name} and I work at ${this.department}.`;
  }
}

let jovi = new Employee('Guangchu', 'Consulting');
console.log(jovi.selfIntro());

// constructor may also be marked *protected*
// class cannot be instatiated outside of its containing class,
// ...but can be extended.
class ProtectedPerson {
  protected name: string;

  protected constructor(name: string) {
    this.name = name;
  }
}

class ProtectedEmployee extends ProtectedPerson {
  private dept: string;

  constructor(name: string, dept: string) {
    super(name);
    this.dept = dept;
  }

  public selfIntro() {
    return `Hey everybody, I'm ${this.name} and I'm wroking at ${this.dept}.`;
  }
}

let guangchu = new ProtectedEmployee('Jovi', 'Tech Delivery');
console.log(guangchu.selfIntro());
