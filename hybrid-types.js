function getCounter() {
    var counter = function (start) {
        var res = 'start at: ' + start;
        return res;
    };
    counter.interval = 7;
    counter.reset = function () { };
    return counter;
}
var cntr = getCounter();
cntr(2037);
cntr.reset();
cntr.interval = 5.0;
console.log('counter: ', cntr(2037));
