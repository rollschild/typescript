var Grid = /** @class */ (function () {
    function Grid(scale) {
        this.scale = scale;
    }
    Grid.prototype.distanceFromOrigin = function (point) {
        var xDist = point.x - Grid.origin.x;
        var yDist = point.y - Grid.origin.y;
        return Math.sqrt(xDist * xDist + yDist * yDist) / this.scale;
    };
    Grid.origin = { x: 0, y: 0 }; // universal across the class
    return Grid;
}());
var grid1 = new Grid(1.0);
var grid2 = new Grid(3.0);
console.log('for grid1: ', grid1.distanceFromOrigin({ x: 3.0, y: 4.0 }));
console.log('for grid2: ', grid2.distanceFromOrigin({ x: 6, y: 8 }));
console.log('origin: ', Grid.origin);
