var Clock = /** @class */ (function () {
    function Clock(month, year) {
        this.currentDate = new Date();
    }
    Clock.prototype.setDate = function (date) {
        this.currentDate = date;
    };
    return Clock;
}());
/* class has static side and instance side
 * ...when a class implements an interface,
 * ...only the instance side of the class is check.
 */
