interface SearchFunc {
  (source: string, subString: string): boolean;
}

let mySearch: SearchFunc = function(src: string, sub: string): boolean {
  return src.search(sub) > -1 ? true : false;
};

let myNewSearch: SearchFunc = function(src, sub) {
  return src.search(sub) > -1;
};
