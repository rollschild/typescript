var ComparisonResult;
(function (ComparisonResult) {
    ComparisonResult[ComparisonResult["LessThan"] = 0] = "LessThan";
    ComparisonResult[ComparisonResult["Equal"] = 1] = "Equal";
    ComparisonResult[ComparisonResult["GreaterThan"] = 2] = "GreaterThan";
})(ComparisonResult || (ComparisonResult = {}));
function max(iter) {
    let iterator = iter[Symbol.iterator]();
    let current = iterator.next();
    if (current.done)
        return undefined;
    let result = current.value;
    while (true) {
        current = iterator.next();
        if (current.done)
            return result;
        if (current.value.compareTo(result) === ComparisonResult.GreaterThan) {
            result = current.value;
        }
    }
}
