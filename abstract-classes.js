var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Department = /** @class */ (function () {
    function Department(name) {
        this.name = name;
    }
    Department.prototype.printName = function () {
        console.log('Department name: ', this.name);
    };
    return Department;
}());
var EngineeringDept = /** @class */ (function (_super) {
    __extends(EngineeringDept, _super);
    function EngineeringDept(deptName) {
        return _super.call(this, deptName) || this;
    }
    EngineeringDept.prototype.printMeeting = function () {
        console.log('10:00 am every Wednesday maybe?');
    };
    EngineeringDept.prototype.generateReports = function () {
        console.log('Generating reports...');
    };
    return EngineeringDept;
}(Department));
var dept = new EngineeringDept('Engineering');
dept.printMeeting();
dept.generateReports();
dept.printName();
