interface LabelValue {
  readonly label: string;
}

function printLabel(labelObj: LabelValue): void {
  console.log(labelObj.label);
}

let obj = {size: 12, label: 'nike', price: '$99'};
printLabel(obj);
let anotherObj: LabelValue = {label: 'onitsuka tiger'};
printLabel(anotherObj);
// ERROR:
// anotherObj.label = 'puma';
