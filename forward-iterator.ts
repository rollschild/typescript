interface IWritable<T> {
  set(value: T): void;
}

interface IReadable<T> {
  get(): T;
}

interface IIncrementable<T> {
  increment(): void;
}

interface IForwardIterator<T>
  extends IReadable<T>,
    IWritable<T>,
    IIncrementable<T> {
  equals(other: IForwardIterator<T>): boolean;
  clone(): IForwardIterator<T>;
}

class LinkedListNode<T> {
  value: T;
  next: LinkedListNode<T> | undefined;

  constructor(value: T) {
    this.value = value;
  }
}

class LinkedListIterator<T> implements IForwardIterator<T> {
  private node: LinkedListNode<T> | undefined;

  constructor(node: LinkedListNode<T> | undefined) {
    this.node = node;
  }

  get(): T {
    if (this.node == undefined) throw Error();

    return this.node.value;
  }

  increment(): void {
    if (this.node == undefined) throw Error();

    this.node = this.node.next;
  }

  set(value: T): void {
    if (this.node == undefined) throw Error();

    this.node.value = value;
  }

  clone(): IForwardIterator<T> {
    return new LinkedListIterator(this.node);
  }

  equals(other: LinkedListIterator<T>): boolean {
    return this.node == (<LinkedListIterator<T>>other).node;
  }
}

function find<T>(
  begin: IForwardIterator<T>,
  end: IForwardIterator<T>,
  pred: (value: T) => boolean
): IForwardIterator<T> {
  while (!begin.equals(end)) {
    if (pred(begin.get())) {
      return begin;
    }

    begin.increment();
  }

  return end;
}
