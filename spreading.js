var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var defaults = { food: 'shrimp', price: '$$$', ambiance: 'noisy' };
var newFood = __assign({}, defaults, { food: 'fish' });
console.log(newFood); // food: fish;
var anotherFood = __assign({ food: 'fish' }, defaults);
console.log(anotherFood); // food: shrimp
var C = /** @class */ (function () {
    function C() {
        this.p = 2037;
    }
    C.prototype.func = function () { }; // non-enumerable
    return C;
}());
var c = new C();
var clone = __assign({}, c);
console.log(clone.p);
// clone.func(); // ERROR
