// *angle bracket* and *as* syntax
// No runtime impact; purely done by compiler
var someValue = "it's a string believe it or not..";
var strLength = someValue.length;
var otherValue = 'yet another string...';
var otherLength = otherValue.length;
