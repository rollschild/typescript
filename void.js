function warnUser() {
    // does NOT return a value
    console.log('This is a WARNING!');
}
warnUser();
// you can ONLY assign *undefined* or *null* to void
var unusable = undefined;
console.log(unusable);
