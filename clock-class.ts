interface ClockConstructor {
  new (hour: number, minute: number);
}
interface ClockInterface {
  tick(): void;
}

const Clock: ClockConstructor = class Clock implements ClockInterface {
  constructor(h: number, m: number) {}

  tick() {
    console.log('di da di..');
  }
};

let someClock = new Clock(20, 37);
someClock.tick();
