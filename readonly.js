var Octopus = /** @class */ (function () {
    function Octopus(name) {
        this.numOfLegs = 8;
        this.name = name;
    }
    return Octopus;
}());
var octo = new Octopus('someOcto');
console.log(octo.numOfLegs);
var OctopusRevised = /** @class */ (function () {
    function OctopusRevised(name) {
        this.name = name;
        this.numOfLegs = 8;
    }
    return OctopusRevised;
}());
var leo = new OctopusRevised('Leo');
console.log(leo.numOfLegs);
