function* generateRandomNumbers() {
    while (true) {
        yield Math.random();
    }
}
let iter = generateRandomNumbers();
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);
function* square(iter) {
    for (const value of iter) {
        yield Math.pow(value, 2);
    }
}
function* take(iter, n) {
    for (const value of iter) {
        if (n-- <= 0)
            return;
        yield value;
    }
}
const values = take(square(generateRandomNumbers()), 6);
for (const value of values)
    console.log(value);
