function buildName(lastName: string, ...otherNames: string[]): string {
  return otherNames.join(' ') + ' ' + lastName;
}

console.log(buildName('Shi', 'Guangchu', 'Jovi'));

let nameBuilder: (
  familyName: string,
  ...givenNames: string[]
) => string = buildName;

console.log(nameBuilder('Wu', 'Junyao', 'Miko'));
