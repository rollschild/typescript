var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person(name) {
        this.name = name;
    }
    return Person;
}());
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(name, department) {
        var _this = _super.call(this, name) || this;
        _this.department = department;
        return _this;
    }
    // members declared *protected* can be accessed within deriving classes
    Employee.prototype.selfIntro = function () {
        return "Hi, my name is " + this.name + " and I work at " + this.department + ".";
    };
    return Employee;
}(Person));
var jovi = new Employee('Guangchu', 'Consulting');
console.log(jovi.selfIntro());
// constructor may also be marked *protected*
// class cannot be instatiated outside of its containing class,
// ...but can be extended.
var ProtectedPerson = /** @class */ (function () {
    function ProtectedPerson(name) {
        this.name = name;
    }
    return ProtectedPerson;
}());
var ProtectedEmployee = /** @class */ (function (_super) {
    __extends(ProtectedEmployee, _super);
    function ProtectedEmployee(name, dept) {
        var _this = _super.call(this, name) || this;
        _this.dept = dept;
        return _this;
    }
    ProtectedEmployee.prototype.selfIntro = function () {
        return "Hey everybody, I'm " + this.name + " and I'm wroking at " + this.dept + ".";
    };
    return ProtectedEmployee;
}(ProtectedPerson));
var guangchu = new ProtectedEmployee('Jovi', 'Tech Delivery');
console.log(guangchu.selfIntro());
