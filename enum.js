var Car;
(function (Car) {
    Car[Car["Audi"] = 16] = "Audi";
    Car[Car["Mazda"] = 2037] = "Mazda";
    Car[Car["Toyota"] = 2038] = "Toyota";
    Car[Car["BMW"] = 2039] = "BMW";
})(Car || (Car = {}));
var car = Car.Mazda;
console.log(car);
var anotherCarName = Car[2038]; // search
// ERROR:
// type *string* NOT assignable to type *Car*
// let anotherCar: Car = Car[2038];
// console.log(anotherCar);
console.log(anotherCarName);
