let passcode = 'randompassword';
class Employee {
    get fullName() {
        return this._fullName;
    }
    set fullName(newName) {
        if (passcode && passcode === 'randompassword') {
            this._fullName = newName;
        }
        else {
            console.log('Error: you are NOT authorized!');
        }
    }
}
let harry = new Employee();
harry.fullName = 'Harry Lowe';
if (harry.fullName)
    console.log(harry.fullName);
