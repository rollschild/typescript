class FluentIterable<T> {
  iter: Iterable<T>;

  constructor(iter: Iterable<T>) {
    this.iter = iter;
  }

  private *mapImpl<U>(func: (item: T) => U): IterableIterator<U> {
    for (const value of this.iter) {
      yield func(value);
    }
  }

  map<U>(func: (item: T) => U): FluentIterable<U> {
    return new FluentIterable(this.mapImpl(func));
  }

  private *filterImpl(pred: (item: T) => boolean): IterableIterator<T> {
    for (const value of this.iter) {
      if (pred(value)) yield value;
    }
  }

  filter(pred: (item: T) => boolean): FluentIterable<T> {
    return new FluentIterable(this.filterImpl(pred));
  }
}
