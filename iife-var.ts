for (var i = 0; i < 10; i++) {
  // capture the current state of *i*
  // by invoking a function with its current value
  (function(i) {
    setTimeout(function() {
      console.log(i);
    }, 100 * i);
  })(i);
}

/*
 * *let* is lexical- or block- scoped
 */

// *let* creates a new scope per iteration
for (let j = 0; j < 10; j++) {
  setTimeout(() => {
    console.log(j);
  }, 100 * j);
}
