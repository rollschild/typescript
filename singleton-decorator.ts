class Widget {}

type WidgetFactory = () => Widget;

function makeWidget(): Widget {
  return new Widget();
}

function singletonDecorator(factory: WidgetFactory): WidgetFactory {
  let instnace: Widget | undefined = undefined;

  return (): Widget => {
    if (instnace == undefined) {
      console.log("new object");
      instnace = factory();
    }

    return instnace;
  };
}

function useTenWidgets(factory: WidgetFactory) {
  for (let i = 0; i < 10; i++) {
    let widget = factory();
  }
}

useTenWidgets(singletonDecorator(makeWidget));
