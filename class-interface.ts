interface ClockInterface {
  currentDate: Date;
  setDate(date: Date): void;
}

class Clock implements ClockInterface {
  currentDate: Date = new Date();
  setDate(date: Date): void {
    this.currentDate = date;
  }

  constructor(month: number, year: number) {}
}
/* class has static side and instance side
 * ...when a class implements an interface,
 * ...only the instance side of the class is check.
 */
