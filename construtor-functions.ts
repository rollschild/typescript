class Greeter {
  static standardGreeting = 'Hello, honey.';
  greeting: string;
  constructor(message: string) {
    this.greeting = message;
  }
  greet(): string {
    if (this.greeting) {
      return 'Hello, ' + this.greeting;
    } else {
      return Greeter.standardGreeting;
    }
  }
}

let greeterMaker: typeof Greeter = Greeter;
// it holds the class itself
// ...or its constructor function
// it constains all of the static members of Greeter along with
// ...the constructor that creates instances of the Greeter class

greeterMaker.standardGreeting = 'Now you see me?';
let greeter = new greeterMaker('Guangchu');
console.log(greeter.greet());
