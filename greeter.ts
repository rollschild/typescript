function greeter(person: string) {
  // type annotations

  return 'Hey, ' + person;
}

let user = 'Guangchu the user';

document.body.innerHTML = greeter(user);
