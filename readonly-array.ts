let arr: number[] = [12, -1, 2037];
let doNotChange: ReadonlyArray<number> = arr;
let anotherArray: ReadonlyArray<number> = [10, 26];
// doNotChange[1] = -2;
// let newArray: number[] = doNotChange;
arr = anotherArray as number[];
