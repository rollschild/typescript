var Greeter = /** @class */ (function () {
    function Greeter(message) {
        this.greeting = message;
    }
    Greeter.prototype.greet = function () {
        if (this.greeting) {
            return 'Hello, ' + this.greeting;
        }
        else {
            return Greeter.standardGreeting;
        }
    };
    Greeter.standardGreeting = 'Hello, honey.';
    return Greeter;
}());
var greeterMaker = Greeter;
greeterMaker.standardGreeting = 'Now you see me?';
var greeter = new greeterMaker('Guangchu');
console.log(greeter.greet());
