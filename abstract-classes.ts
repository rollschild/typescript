abstract class Department {
  constructor(public name: string) {}

  printName(): void {
    console.log('Department name: ', this.name);
  }

  abstract printMeeting(): void; // muste be implemented in derived classes
}

class EngineeringDept extends Department {
  constructor(deptName: string) {
    super(deptName);
  }

  printMeeting(): void {
    console.log('10:00 am every Wednesday maybe?');
  }

  generateReports(): void {
    console.log('Generating reports...');
  }
}

let dept = new EngineeringDept('Engineering');

dept.printMeeting();
dept.generateReports();
dept.printName();

// The following does NOT work
/* let dev: Department;
 * dev = new EngineeringDept('Dev');
 * dev.generateReports(); */
