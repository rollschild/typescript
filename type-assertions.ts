// *angle bracket* and *as* syntax
// No runtime impact; purely done by compiler
let someValue: any = "it's a string believe it or not..";
let strLength: number = (<string>someValue).length;

// why the color scheme not working?
let otherValue: any = 'yet another string...';
let otherLength: number = (otherValue as string).length;
