var queue = [];
function countDown(counterId, from, callback) {
    console.log(counterId + ": " + from);
    if (from > 0) {
        queue.push(function () { return countDown(counterId, from - 1, callback); });
    }
    else {
        queue.push(callback);
    }
}
;
queue.push(function () { return countDown("counterTwo", 6, function () { return console.log("Done"); }); });
while (queue.length > 0) {
    var func = queue.shift();
    func();
}
