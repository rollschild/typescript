for (var i = 0; i < 10; i++) {
    // capture the current state of *i*
    // by invoking a function with its current value
    (function (i) {
        setTimeout(function () {
            console.log(i);
        }, 100 * i);
    })(i);
}
var _loop_1 = function (j) {
    setTimeout(function () {
        console.log(j);
    }, 100 * j);
};
/*
 * *let* is lexical- or block- scoped
 */
for (var j = 0; j < 10; j++) {
    _loop_1(j);
}
