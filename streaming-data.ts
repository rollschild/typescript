function* generateRandomNumbers(): IterableIterator<number> {
  while (true) {
    yield Math.random();
  }
}

let iter: IterableIterator<number> = generateRandomNumbers();
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);

function* square(iter: Iterable<number>): IterableIterator<number> {
  for (const value of iter) {
    yield value ** 2;
  }
}

function* take(iter: Iterable<number>, n: number): IterableIterator<number> {
  for (const value of iter) {
    if (n-- <= 0) return;

    yield value;
  }
}

const values: IterableIterator<number> = take(
  square(generateRandomNumbers()),
  6
);

for (const value of values) console.log(value);
