function warnUser(): void {
  // does NOT return a value
  console.log('This is a WARNING!');
}

warnUser();

// you can ONLY assign *undefined* or *null* to void
let unusable: void = undefined;
console.log(unusable);
