class Octopus {
  readonly name: string;
  readonly numOfLegs: number = 8;

  constructor(name: string) {
    this.name = name;
  }
}

let octo = new Octopus('someOcto');
console.log(octo.numOfLegs);

class OctopusRevised {
  readonly numOfLegs: number = 8;

  constructor(readonly name: string) {}
}

let leo = new OctopusRevised('Leo');
console.log(leo.numOfLegs);
