function greeter(person) {
    return 'Hello, ' + person.firstName + ' ' + person.lastName;
}
var user = { firstName: 'Jovi', lastName: 'Shi' };
document.body.innerHTML = greeter(user);
