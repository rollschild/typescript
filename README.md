# typescript

Repo for learning and practicing TypeScript.

## Numerical Types

- Two defining characteristics:
  - width
  - encoding
- Encoding:
  - unsigned binary
  - two's complement
  - IEEE 754
    - representing floating point numbers
    - in JavaScript and TypeScript, numbers are represented as 64-bit
      floating-point using the **binary64** encoding
- arithmetic overflow vs. arithmetic underflow
- To handle overflow:
  - wrap around
  - saturation
  - error out
- For floating-point numbers
  - sign
  - exponent
  - mantissa
- `NaN` - not a number
- If precision is needed, for example dealing with currency - avoid using
  floating point numbers
  - small integer numbers can safely be represented without rounding
  - it's a better idea to encode a price as a pair of dollars and cents
    integers
  - JavaScript has a `Number.isSafeInteger()`, which tells whether an integer
    value can be represented without rounding
- because of rounding, it's not a good idea to compare floating-point numbers
  - but we can make sure their difference is within a given threshold
  - which, should be the maximum rounding error
  - this value is called **machine epsilon**
  - and is encoding-specific
  - and is provided by JavaScript as `Number.EPSILON`

## Encoding Text

- the Unicode standard works with two similar but distinct concepts:
  - characters
    - when encoding text
  - graphemes
    - when rendering text
- **glyph**: a particular representation of a character
- a **grapheme** is an indivisible unit
  - a grapheme can be represented by different glyphs
- each Unicode character is defined as a code point
- Variable-length encodings:
  - UTF-16 and UTF-8
- `grapheme-splitter`
- three common errors:
  - manipulating encoded text at character level instead of grapheme level
  - at byte level instead of character level
  - interpreting a sequence of bytes as text with the wrong encoding

## Composition Types

- `Number.isSafeInteger`
- record:
  - check if any invariants that need to be enforced
  - if so, remember to make the memebers private
    - an alternative is to make members `readonly` - immutable
    - the advantage of immutable data is thread safety - it prevents data races
- Optional types
  - an optional type represents an optional value of type `T`
  - an instance of optional type `T` can hold a value (any) of type `T` or a special value indicating the absence of
    a vlue of type `T`
  - vs. _nullable type_
- result as error
  - we should return the result _either_ as an error _or_ as a valid value
  - throwing an exception on error is a perfectly valid example of result or error: the funtion either returns a valid
    result or throws an exception
  - In several situations, exceptions cannot be used and an `Either` type is preferred:
    - when propagating errors across processes or threads
    - when dealing with user inputs
    - when callnig OS APIs that use error codes

## Variant Types

- a.k.a. _tagged union types_, contain a value of any number of underlying types

## Visitor pattern

- _double-dispatch mechanism_

## Algebraic data types

- ADT
  - provides two ways to combine types:
    - product types
    - sum types

## Type safety

- `unique symbol`: other objects with the same shape _cannot_ be interpreted as this type
  - **only** allowed on `const` declarations and `readonly static` properties
  - in order to reference a `unique symbol` you will have to use `typeof`

```
declare const NsType: unique symbol;

class Ns {
  readonly value: number;
  [NsType]: void;

  constructor(value: number) {
    this.value = value;
  }
}
```

- the _primitive obsession antipattern_
  - when we rely on basic types to represent everything
- a **factory** is a class or function whose main job is to create another object

  - make the constructor of a class to be `private` - anything outside could not create an instance of it
  - then make a `static makeInstance` method
    - return a valid instance or `undefined`
    - because, constructor itself _cannot return_

- Type cast
  - _explicit type cast_: allows us to tell compiler to treat a value as though it had a certain type
    - in TypeScript, adding `<NewType>` in front of the value
    - or, adding `as NewType` after the value
    - `<SportsCar><unknown>myBike`
      - You would need to cast `myBike` to `unknown` first
      - because `Bike` and `SportsCar` don't overlap - a valid value of one type can never be a valid value of the other
  - _implicit type cast_: also known as _coercion_
    - type cast performed automatically by compiler
    - _upcast_: interpreting a derived class as a base class
    - _downcast_: casting from a parent class to a derived class
      - not safe
    - _widening cast_: from an integer with a fixed number of bits to another type that represents values with more bits
    - _narrowing cast_: dangerous
  - serialization and deserialization
    - `JSON.stringify()`
    - `JSON.parse()`
    - `const deserializedObj: Obj = <Obj>Object.assign(new Obj(), JSON.parse(serializedObj))`

## Function Types

- Strategy Pattern
  - one of the most commonly used design patterns
- the type of a function is given by they type of its arguments and its return type
- first-class functions
  - the ability to assign functions to variables and treat them like any other values in the type system
- lazy evaluation
- _lambda_: anonymous function
- first-order function: a "normal" function that accepts one or more nonfunction arguments and returns a nonfunction
  type
- second-order function: a function that takes a first-order function as an argument or returns a first-order function
- _higher-order functions_
- `reduce()` and `reduceRight()`
- _monoid_
  - _identity_
  - _associativity_
  - if a set `T` with an operation `op` hsa an identity element and the operation is associative, the resulting
    algebraic structure is called a _monoid_

## Advanced applications of function types

- decorator pattern
  - a behavioral software design pattern that extends the behavior of an object without modifying the class of the
    object
  - it supports the _single-responsibility principle_
    - a class should have just one responsibility
- Closure
  - _lambda capture_: an external variable captured within a lambda
  - lambda captures implemented through *closure*s
  - closure records the environment in which the function was created - it can manage state between calls
  - closure only makes sense if we have higher-order functions
  - an object-oriented counter keeps track of state via private member
  - a functional counter keeps track of state in its captured context
  - _resumable function_: keeps track of its own state and, whenever it gets called, does not run from the beginning
    - it resumes executing from where it left off the last time it returned
- JavaScript is single threaded
  - asynchronous execution is achieved by the run time with an event loop
- the `readline-sync` package
  - `readlineSync.question()`
- a _callback_ is a function that we provide to an asynchronous function as an argument
- the async function does _not_ block execution
  - the next line of code gets executed
- Async execution can be achieved with threads or with an event loop
- _Threads_
  - on POSIX, new threads are created with `pthread_create()`
  - managed by thread scheduler
- Event loop
  - uses a _queue_
  - async functions get enqueued, and they themselves can enqueue other functions
  - as long as the queue is not empty, the first function in line gets dequeued and executed

## Promises

- a _promise_ is a proxy for a value that will be available at a future point
  in time
  - until the code that produces the value runs, other code can use the promise
    to setup how the value will be processed when it arrives, what to do in
    case of error, and even to cancel the future execution
  - a function setup to be called when the result of a promise is available is
    called a _continuation_
- a promise can be in three states:
  - pending
  - settled
  - rejected
- a continuation does _not_ have to return promise - we don't always chain
  asynchronous functions
- There're a couple more ways to schedule the execution of asynchronous
  functions via:
  - `Promise.all()`
  - `Promise.race()`
- `Promise.all()` takes as arguments a set of promises and returns a promise
  that is settled when **all** the provided promises are settled
- `Promise.race()` takes a set of promises and returns a promise that is
  settled when **any one** of the promises is settled

## async/await

- can appear only in a function that returns a `Promise`
- remember to wrap the `await` call in a `try` block to catch the expected
  errors!

## Subtyping

- a type `S` is a subtype of type `T` if an instance of `S` can be safely used anywhere an instance of `T` is expected
- **Liskov substitution principle**
- Two ways in which subtyping relationships are established:
  - **nominal subtyping**
  - **structural subtyping**
- to enforce nominal subtyping in TypeScript, we can use `unique symbol`
- `unique symbol` generates a "name" that is guaranteed to be unique across all the code
  - different `unique symbol` declarations will generate different names, and no user-declared name can ever match
    a generated name
- `unknown` vs. `any`:
  - can assign anything to `unknown`
  - can assign anything to `any`; can also assign `any` to anything
- **top type**: a type to which we can assign any value
  - any other type is a subtype of the top type
  - `Object` is the supertype of _most_ types, with two execptions:
    - `null`
    - `undefined`
  - the supertype of absolutely anything: `Object | null | undefined`
    - which is, `unknown`
- `never` is the type that cannot be assigned any value
- **bottom type**: subtype of any other types
  - `never` is a bottom type
  - an empty type: a type for which we cannot create an actual value

## Allowed substitutions

- For sum types, the supertype has more types than the subtype
- arrays preserve the subtyping relationsship of the underlying types that they are storing
- covariance
  - a type that preserves the subtyping relationship of its underlying type is called **covariant**
  - functions are covariant in their return types

```
class ShapeMaker {
  make(): Shape {
    return new Shape();
  }
}

class TriangleMaker extends ShapeMaker {
  make(): Triangle {
    return new Triangle();
  }
}
```

- **contravariance**: a type that reverses the subtyping relationship of its underlying type is called
  **contravariant**
  - In most programming languages, functions are contravariant with regard to their arguments
  - one exception is TypeScript:
    - if `Triangle` is a subtype of `Shape`, a function of type `(argument: Shape) => void` and a function of type
      `(argument: Triangle) => void` can be substituted for each other
    - they are subtypes of each other
    - **bivariance**
- types are bivariant if, from the subtyping relationship of their underlying types, they become subtypes of each other

## Object-oriented Programming

- OOP: a paradigm based on the concept of objects, which contain both data and code
  - data is the state of the object
  - code is one or more methods, a.k.a. _messages_
- Abstract classes:

```
abstract class Logger {
  abstract log(line: string): void;
}
```

- Abstract class vs. Interface
  - we should prefer `interface` over abstract class, because `interface` specifies a **contract**
  - An `interface`, or a _contract_, is a description of a set of messages that are understood by any object implementing
    that `interface`
    - an `interface` does not have any state
    - not an **is-a relationship**
    - it just specifies/satisfies a contract
  - an abstract class can do much more
    - it can contain nonabstract methods or state
    - the _only_ difference between an abstract class and a concrete one is that we **cannot** directly create an
      instance of the abstract class
    - **is-a relationship**
- The general OOP principle: **code against interfaces**
- Usually it's good to have the children be concrete classes and all parents up the hierarchy be abstract
- Another important rule of OOP: **prefer composition over inheritance whenever possible**
- Composition: **has-a**
  - all the state coming from component properties is encapsulated in those components
  - the Adapter pattern
- mix-in
  - it establishes an **includes** relationship between a type and its mixed-in type
  - very useful for reducing boilerplate code
  - best to implement _cross-cutting concerns_

## Generic data structures

- **identity function**: f(x) = x
- type parameter: an identifier for a generic type name
- Generic types: a _generic type_ is a generic function/class/interface/etc that is parameterized over one or more types

```
class BinaryTreeNode<T> {
  value: T;
  left: BinaryTreeNode<T> | undefined;
  right: BinaryTreeNode<T> | undefined;

  constructor(value: T) {
    this.value = value;
  }
}
```

- What is a data structure?
  - the data itself
  - the shape of the data
  - a set of shape-preserving operations
- Iterator
  - an object that enables traversal of a data structure
- TypeScript provides an `IterableIterator<T>` interface, which is a combination of `Iterable` and `Iterator`
  - can be iterated manually with `next()` but can also be used in a `for...of` statement
- `IterableIterator<T>` is evaluated lazily
  - only when we start consuming values in the `for...of` loop do values start flowing through
- `Iterator<T>` represents an iterator; whereas `Iterable<T>` represents something can be iterated over

## Generic algorithms and iterators

- _predicate_: a function that takes one argument of some type and returns a `boolean`
- constraints on type parameters
  - constraints inform the compiler about the capabilities that a type argument must have

```
interface IRenderable {
  render(): void;
}

function renderAll<T extends IRenderable>(iter: Iterable<T>): void {
  for (const item of ier) {
    item.render();
  }
}
```

- **input iterator**: an iterator that can traverse a sequence once and provide its value
- **output iterator**: an iterator that can traverse a sequence and write values to it
- **forward iterator**: an iterator that can be advanced, can read the value at its current position, and update that value
  - it can also be cloned, so that advancing one copy of the iterator does not advance the clone
- **bidirectional iterator**: has the same capabilities as a forward iterator plus the capability to be decremented
- **random-access iterator**: can jump forward and backward any given number of elements in constant time
  - enables the most efficient algorithms

## Higher kinded types and beyond

- TypeScript does **not** support function overloading
- **Functor**
  - a functor is a generalization of functions that perform mapping operations
  - the general definition of a functor relies on higher kinded types
- **Higher kinded types**
  - a generic type is a type that has a type parameter, such as a generic type `T`, or a type like `Box<T>` that has a type
    parameter `T`
  - a **higher kinded type** just like a higher-order function, represents a type parameter with another type parameter
  - Such as `T<U>` or `Box<T<U>>`
- Type constructor
  - a function that returns a type
  - every type has a constructor
    - `() -> [number type]`
  - for generics, such as `T`
    - `(T) -> [T type]`
- A functor is any type `H` with a type parameter `T` (`H<T>`) for which we have a function `map()` that takes an
  argument of type `H<T>` and a function from `T` to `U`
  - and returns `H<U>`

```
interface Functor<T> {
  map<U>(func: (value: T) => U): Functor<U>;
}
```

- functor for functions

```
namespace Function {
  export function map<T, U>(
    f: (arg1: T, arg2: T) => T,
    func: (value: T) => U
  ): (arg1: T, arg2: T) => U {
    return (arg1: T, arg2: T) => func(f(arg1, arg2));
  }
}
```

- `bind()` vs. `map()`
  - `map()` takes `Box<T>` and a function from `T` to `U` and returns `Box<U>`
  - `bind()` takes `Box<T>` and a function from `T` to `Box<U>` and returns `Box<U>`

```
namespace Box {
  export function bind<T, U>(
    box: Box<T>,
    func: (value: T) => Box<U>
  ): Box<U> {
    return func(box.value);
  }
}
```

- Monad
  - a **monad** is a generic type `H<T>` for which we have
    - a function like `unit()` that takes a value of type `T` and
      returns a value of type `H<T>`
    - and a function like `bind()` that takes a value of type `H<T>` and a function from `T` to `H<U>` and returns
      a value of type `H<U>`
  - Promise is monadic
    - `.then()` is the Promise's version of `bind()`
