let defaults = {food: 'shrimp', price: '$$$', ambiance: 'noisy'};
let newFood = {...defaults, food: 'fish'};
console.log(newFood); // food: fish;
let anotherFood = {food: 'fish', ...defaults};
console.log(anotherFood); // food: shrimp

class C {
  p = 2037;
  func() {} // non-enumerable
}
let c = new C();
let clone = {...c};
console.log(clone.p);
// clone.func(); // ERROR
