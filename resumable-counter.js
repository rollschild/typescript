function* counter() {
    let c = 0;
    while (true) {
        yield ++c;
    }
}
const realCounter = counter();
console.log(realCounter.next().value);
console.log(realCounter.next().value);
console.log(realCounter.next().value);
console.log(realCounter.next().value);
console.log(realCounter.next().value);
