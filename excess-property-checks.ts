interface SquareConfig {
  color?: string;
  width?: number;
}

// Method 2
interface NewSquareConfig {
  color?: string;
  width?: number;
  [propName: string]: any;
} // can have any number of properties,
// ...and so long as they are not color or width,
// ...their types do not matter

function createSquare(config: SquareConfig): {color: string; area: number} {
  return {color: config.color, area: config.width * config.width};
}

// Method 1
let mySquare = createSquare({colour: 'purple', width: 12} as SquareConfig);
console.log(mySquare);

// Method 3
// works ONLY when squareOptions has at least one common property
// with SquareConfig
let squareOptions = {colour: 'green', width: 11};
let newSquare = createSquare(squareOptions);
console.log(newSquare);
