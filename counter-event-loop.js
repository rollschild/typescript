var queue = [];
function countDown(counterId, from) {
    console.log(counterId + ": " + from);
    if (from > 0) {
        queue.push(function () { return countDown(counterId, from - 1); });
    }
}
queue.push(function () { return countDown("counterOne", 6); });
while (queue.length > 0) {
    var func = queue.shift();
    func();
}
