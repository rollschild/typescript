// how to compile this file:
// tsc --target ES2016 accessors.ts
let passcode = 'randompassword';

class Employee {
  private _fullName: string;

  get fullName(): string {
    return this._fullName;
  }

  // this setter cannot have a return type
  set fullName(newName: string) {
    if (passcode && passcode === 'randompassword') {
      this._fullName = newName;
    } else {
      console.log('Error: you are NOT authorized!');
    }
  }
}

let harry = new Employee();
harry.fullName = 'Harry Lowe';
if (harry.fullName) console.log(harry.fullName);
