class Grid {
  static origin = {x: 0, y: 0}; // universal across the class

  distanceFromOrigin(point: {x: number; y: number}): number {
    let xDist = point.x - Grid.origin.x;
    let yDist = point.y - Grid.origin.y;

    return Math.sqrt(xDist * xDist + yDist * yDist) / this.scale;
  }

  constructor(public scale: number) {}
}

let grid1 = new Grid(1.0);
let grid2 = new Grid(3.0);

console.log('for grid1: ', grid1.distanceFromOrigin({x: 3.0, y: 4.0}));
console.log('for grid2: ', grid2.distanceFromOrigin({x: 6, y: 8}));
console.log('origin: ', Grid.origin);
