function keepWholeObject(wholeObj: {a: string; b?: number}) {
  let {a, b = 2037} = wholeObj;
  console.log(a);
  // *b* is optional
  // question mark *?* indicates this parameter is optional
}
keepWholeObject({a: 'hello', b: 16});
